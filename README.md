# GoF
常见设计模式模板及测试用例

## 目录

#### 模板
- com.adamjwh.gof.singleton 单例模式
- com.adamjwh.gof.factory_method 工厂方法模式
- com.adamjwh.gof.abstract_factory 抽象工厂模式
- com.adamjwh.gof.builder 建造者模式
- com.adamjwh.gof.prototype 原型模式
- com.adamjwh.gof.adapter 适配器模式（对象适配器）
- com.adamjwh.gof.adapter2 适配器模式（类适配器）
- com.adamjwh.gof.bridge 桥接模式
- com.adamjwh.gof.composite 组合模式（透明模式）
- com.adamjwh.gof.composite2 组合模式（安全模式）
- com.adamjwh.gof.decorator 装饰模式
- com.adamjwh.gof.facade 外观模式
- com.adamjwh.gof.flyweight 享元模式
- com.adamjwh.gof.proxy 代理模式
- com.adamjwh.gof.proxy.static_proxy 代理模式（静态代理）
- com.adamjwh.gof.proxy.dynamic_proxy 代理模式（动态代理）
- com.adamjwh.gof.proxy.cglib_proxy 代理模式（CGLIB代理）
- com.adamjwh.gof.observer 观察者模式
- com.adamjwh.gof.templatemethod 模板方法模式
- com.adamjwh.gof.command 命令模式
- com.adamjwh.gof.state 状态模式
- com.adamjwh.gof.chain_of_responsibility 职责链模式（责任链模式）
- com.adamjwh.gof.interpreter 解释器模式
- com.adamjwh.gof.mediator 中介者模式
- com.adamjwh.gof.visitor 访问者模式
- com.adamjwh.gof.strategy 策略模式
- com.adamjwh.gof.memento 备忘录模式
- com.adamjwh.gof.iterator 迭代器模式

#### 测试用例
- com.adamjwh.gofex.singleton 单例模式
- com.adamjwh.gofex.factory_method 工厂方法模式
- com.adamjwh.gofex.abstract_factory 抽象工厂模式
- com.adamjwh.gofex.builder 建造者模式
- com.adamjwh.gofex.prototype 原型模式
- com.adamjwh.gofex.adapter 适配器模式
- com.adamjwh.gofex.bridge 桥接模式
- com.adamjwh.gofex.composite 组合模式
- com.adamjwh.gofex.decorator 装饰模式
- com.adamjwh.gofex.facade 外观模式
- com.adamjwh.gofex.flyweight 享元模式
- com.adamjwh.gofex.proxy 代理模式
- com.adamjwh.gofex.observer 观察者模式
- com.adamjwh.gofex.templatemethod 模板方法模式
- com.adamjwh.gofex.command 命令模式
- com.adamjwh.gofex.state 状态模式
- com.adamjwh.gofex.chain_of_responsibility 职责链模式（责任链模式）
- com.adamjwh.gofex.interpreter 解释器模式
- com.adamjwh.gofex.mediator 中介者模式
- com.adamjwh.gofex.visitor 访问者模式
- com.adamjwh.gofex.strategy 策略模式
- com.adamjwh.gofex.memento 备忘录模式